from .ldap_auth import LDAPAuth as Auth

__all__ = ('Auth',)
