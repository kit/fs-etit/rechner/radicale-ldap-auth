import ssl

from ldap3 import Server, Connection, Tls
from radicale import config
from radicale.auth import BaseAuth
from radicale.log import logger

PLUGIN_CONFIG_SCHEMA = {
    'auth': {
        'ldap_host': {'value': '', 'type': str},
        'ldap_tls': {'value': True, 'type': bool},
        'ldap_bind_dn': {'value': '', 'type': str},
        'ldap_bind_pw': {'value': '', 'type': str},
        'ldap_user_base': {'value': '', 'type': str},
        'ldap_user_filter': {'value': '(&(objectClass=posixAccount)(uid={login}))', 'type': str},
        'ldap_group_base': {'value': '', 'type': str},
        'ldap_group_filter': {'value': '', 'type': str},
        'ldap_group_member_attribute': {'value': 'memberUid', 'type': str},
    }
}


class LDAPAuth(BaseAuth):
    def __init__(self, configuration: 'config.Configuration'):
        super().__init__(configuration.copy(PLUGIN_CONFIG_SCHEMA))

        ldap_host = self.configuration.get('auth', 'ldap_host')
        ldap_tls = self.configuration.get('auth', 'ldap_tls')

        tls = Tls(validate=ssl.CERT_REQUIRED)
        self.server = Server(ldap_host, use_ssl=ldap_tls, tls=tls)

        self.ldap_bind_dn = self.configuration.get('auth', 'ldap_bind_dn')
        self.ldap_bind_pw = self.configuration.get('auth', 'ldap_bind_pw')

        self.ldap_user_base = self.configuration.get('auth', 'ldap_user_base')
        self.ldap_user_filter = self.configuration.get('auth', 'ldap_user_filter')

        self.ldap_group_base = self.configuration.get('auth', 'ldap_group_base')
        self.ldap_group_filter = self.configuration.get('auth', 'ldap_group_filter')
        self.ldap_group_member_attribute = self.configuration.get('auth', 'ldap_group_member_attribute')

        if ((self.ldap_group_filter == '' or self.ldap_group_base == '')
                and (self.ldap_group_base != '' or self.ldap_group_base != '')):
            raise RuntimeError('auth config options "ldap_group_base" and "ldap_group_filter" are mutually dependent')

        conn = Connection(self.server, user=self.ldap_bind_dn, password=self.ldap_bind_pw)
        if conn.bind():
            if self._check_connection_result(conn):
                raise RuntimeError('Invalid LDAP bind credentials provided')
        else:
            logger.error('LDAP: could not authenticate with bind credentials')
            raise RuntimeError('Invalid LDAP bind credentials provided')

    @staticmethod
    def _check_connection_result(connection: Connection) -> bool:
        if connection.result["result"] != 0:
            logger.error(connection.result)
            return True
        return False

    def _check_user_exists(self, username: str) -> str:
        """Checks if user exists and returns its dn"""
        search_filter = self.ldap_user_filter.format(login=username)

        with Connection(self.server, user=self.ldap_bind_dn, password=self.ldap_bind_pw) as conn:
            if self._check_connection_result(conn):
                return ''

            conn.search(search_base=self.ldap_user_base, search_filter=search_filter)
            if self._check_connection_result(conn):
                return ''

            if len(conn.response) == 0:
                logger.info(f'LDAP: no user for filter {search_filter} in {self.ldap_user_base} found')
                return ''
            elif len(conn.response) > 1:
                raise RuntimeError(
                    f'LDAP: multiple users found for filter {search_filter} in {self.ldap_user_base}')

            user = conn.response[0]
            return user['dn']

    def _check_user_credentials(self, user_dn: str, password: str) -> bool:
        """Checks for valid credentials"""

        conn = Connection(self.server, user=user_dn, password=password)
        if conn.bind():
            if self._check_connection_result(conn):
                return False
            logger.info(f'LDAP: bind for user {user_dn} successful')
            return True

        return False

    def _check_group_membership(self, username: str) -> bool:
        """Checks if user is in required group"""
        ldap_group_base = self.configuration.get('auth', 'ldap_group_base')
        ldap_group_filter = self.configuration.get('auth', 'ldap_group_filter')
        ldap_group_member_attribute = self.configuration.get('auth', 'ldap_group_member_attribute')

        with Connection(self.server, user=self.ldap_bind_dn, password=self.ldap_bind_pw) as conn:
            if self._check_connection_result(conn):
                return False

            conn.search(
                search_base=ldap_group_base,
                search_filter=ldap_group_filter,
                attributes=[ldap_group_member_attribute]
            )
            if self._check_connection_result(conn):
                return False

            if len(conn.response) == 0:
                logger.info(f'LDAP: no group for filter {ldap_group_filter} in {ldap_group_base} found')
                return False

            group = conn.response[0]
            group_members = group['attributes'].get(ldap_group_member_attribute, [])
            if username in group_members:
                return True
            else:
                logger.info(f'LDAP: user {username} not in group {group["dn"]}')
                return False

    def login(self, login: str, password: str) -> str:
        if login == '' or password == '':
            return ''

        # check if user exists
        user_dn = self._check_user_exists(login)
        if user_dn == '' or user_dn is None:
            return ''

        # check for valid credentials
        if not self._check_user_credentials(user_dn=user_dn, password=password):
            return ''

        # check for group membership, if required
        if self.ldap_group_filter != '':
            if not self._check_group_membership(username=login):
                return ''

        return login
