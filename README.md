# radicale-ldap-auth

Radicale Plugin for authentication against an LDAP with RFC2307 schema

## config
```ini
[auth]
ldap_host = example.com 
ldap_tls = true
ldap_bind_dn = cn=admin,dc=example,dc=com
ldap_bind_pw = foobar
ldap_user_base = ou=People,dc=example,dc=com
ldap_user_filter = (&(objectClass=posixAccount)(uid={login}))
ldap_group_base = ou=Group,dc=example,dc=com
ldap_group_filter = (&(cn=users)(objectClass=posixGroup))
ldap_group_member_attribute = memberUid
```